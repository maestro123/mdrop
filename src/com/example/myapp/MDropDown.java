package com.example.myapp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.*;
import android.view.animation.OvershootInterpolator;
import android.widget.PopupWindow;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by artyom on 16.4.15.
 */
public class MDropDown extends ViewGroup {

    public static final String TAG = MDropDown.class.getSimpleName();

    private final OvershootInterpolator mInterpolator = new OvershootInterpolator();

    private ArrayList<MDropItem> mItems = new ArrayList<>();
    private View mAnchorView;
    private TYPE mType = TYPE.VERTICAL;
    private ALIGN mAlign = ALIGN.CENTER;
    private ColorDrawable mBackgroundDrawable = new ColorDrawable(Color.parseColor("#80000000"));
    private PopupWindow mPopupWindow;
    private int mItemMargin = 30;
    private boolean isVisible = false;
    private boolean isAnchorAtLeft = false;
    private boolean isAnchorAtTop = false;

    public enum TYPE {
        HORIZONTAL, VERTICAL, BOTH
    }

    public enum ALIGN {
        CENTER, LEFT, RIGHT, TOP, BOTTOM
    }

    public MDropDown(Context context) {
        super(context);
        init();
    }

    public MDropDown(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    final void init() {
        mBackgroundDrawable.setAlpha(0);
        setBackgroundDrawable(mBackgroundDrawable);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (mPopupWindow != null && event.getKeyCode() == 4) {
            toggle();
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        if (mAnchorView == null)
            return;

        Log.e(TAG, "size: " + getWidth() + "/" + getHeight());

        ensureAnchorState();

        final int childCount = getChildCount();

        final Rect rect = new Rect();
        getGlobalVisibleRect(rect);

        final Rect anchorRect = new Rect();
        mAnchorView.getGlobalVisibleRect(anchorRect);

        //TODO: fix for not full screen state

        int anchorX = (int) mAnchorView.getX();
        int startX = isAnchorAtLeft && mType == TYPE.HORIZONTAL ? anchorX + mAnchorView.getMeasuredWidth() : anchorX;
        int anchorY = (int) mAnchorView.getY() + (anchorRect.top - (int) mAnchorView.getY() - rect.top);
        int startY = isAnchorAtTop && mType == TYPE.VERTICAL ? anchorY + mAnchorView.getMeasuredHeight() : anchorY;

        for (int i = 0; i < childCount; i++) {
            final View child = getChildAt(i);
            if (child.getVisibility() != GONE) {
                int width = child.getMeasuredWidth();
                int height = child.getMeasuredHeight();

                int fX = 0;
                int fY = 0;

                if (mType == TYPE.HORIZONTAL) {
                    startX += isAnchorAtLeft ? mItemMargin : -mItemMargin;
                    if (mAlign == ALIGN.CENTER) {
                        fY = (mAnchorView.getMeasuredHeight() - height) / 2;
                    } else if (mAlign == ALIGN.TOP) {
                        fY = 0;
                    } else if (mAlign == ALIGN.BOTTOM) {
                        fY = mAnchorView.getMeasuredHeight() - height;
                    }
                } else if (mType == TYPE.VERTICAL) {
                    startY += isAnchorAtTop ? mItemMargin : -mItemMargin;
                    if (mAlign == ALIGN.CENTER) {
                        fX = (mAnchorView.getMeasuredWidth() - width) / 2;
                    } else if (mAlign == ALIGN.LEFT) {
                        fX = 0;
                    } else if (mAlign == ALIGN.RIGHT) {
                        fX = mAnchorView.getMeasuredWidth() - width;
                    }
                }
                child.layout(startX + fX - (!isAnchorAtLeft && mType == TYPE.HORIZONTAL ? width : 0),
                        startY + fY - (!isAnchorAtTop && mType == TYPE.VERTICAL ? height : 0),
                        startX + width + fX,
                        startY + height + fY);

                if (!isAnchorAtTop && mType == TYPE.VERTICAL) {
                    width = -width;
                    height = -height;
                }

                if (!isAnchorAtLeft && mType == TYPE.HORIZONTAL) {
                    width = -width;
                }

                if (mType == TYPE.HORIZONTAL) {
                    startX += width;
                } else if (mType == TYPE.VERTICAL) {
                    startY += height;
                } else {
                    startX += width;
                    startY += height;
                }

                if (!isVisible)
                    initializeItem(i);

            }
        }

    }

    public void toggle() {
        animate(!isVisible);
    }

    public void animate(boolean visible) {
        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            ViewPropertyAnimator animator = child.animate();
            animator.setInterpolator(mInterpolator);
            animator.alpha(visible ? 1f : 0f);
            if (mType == TYPE.HORIZONTAL) {
                int distance = getChildWidthByOffset(i) + mAnchorView.getMeasuredWidth();
                animator.translationX(visible ? 0 : isAnchorAtLeft ? -distance : distance);
            } else if (mType == TYPE.VERTICAL) {
                int distance = getChildHeightByOffset(i) + child.getMeasuredHeight();
                animator.translationY(visible ? 0 : isAnchorAtTop ? -distance : distance);
            }
            if (!visible && mPopupWindow != null && i == getChildCount() - 1) {
                animator.setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        mPopupWindow.dismiss();
                    }
                });
            }
            animator.start();
        }
        ObjectAnimator animator = ObjectAnimator.ofInt(mBackgroundDrawable, "alpha", visible ? 255 : 0);
        animator.start();
        isVisible = visible;
    }

    private final void initializeItem(int itemPosition) {
        final View v = getChildAt(itemPosition);
        v.setAlpha(0);
        int x = 0, y = 0;
        if (mType == TYPE.HORIZONTAL) {
            final int distance = getChildWidthByOffset(itemPosition) + mAnchorView.getMeasuredWidth();
            x = isAnchorAtLeft ? -distance : distance;
        } else if (mType == TYPE.VERTICAL) {
            final int distance = getChildHeightByOffset(itemPosition) + v.getMeasuredHeight();
            y = isAnchorAtTop ? -distance : distance;
        }
        v.setTranslationX(x);
        v.setTranslationY(y);
    }

    public final int getChildWidthByOffset(int offset) {
        int width = 0;
        for (int i = 0; i < offset; i++) {
            width += getChildAt(i).getMeasuredWidth();
        }
        return width;
    }

    public final int getChildHeightByOffset(int offset) {
        int height = 0;
        for (int i = 0; i < offset; i++) {
            height += getChildAt(i).getMeasuredHeight();
        }
        return height;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        for (int i = 0; i < getChildCount(); i++) {
            final View child = getChildAt(i);
            if (child.getVisibility() != GONE) {
                measureChild(child, widthMeasureSpec, heightMeasureSpec);
            }
        }
    }

    public void setAnchorView(View v) {
        mAnchorView = v;
        if (v != null)
            ensureAnchorState();
    }

    public void setPopupWindow(PopupWindow window) {
        mPopupWindow = window;
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle();
            }
        });
    }

    private void ensureAnchorState() {
        int[] windowLocation = new int[2];
        int[] screenLocation = new int[2];

        mAnchorView.getLocationInWindow(windowLocation);
        mAnchorView.getLocationOnScreen(screenLocation);

        final Rect anchorRect = new Rect();
        mAnchorView.getGlobalVisibleRect(anchorRect);

        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        int screenHeight = getResources().getDisplayMetrics().heightPixels;

        isAnchorAtTop = anchorRect.bottom < screenHeight / 2;
        isAnchorAtLeft = anchorRect.left < screenWidth / 2;
    }

    public void addItem(MDropItem item) {
        mItems.add(item);
        addView(item.makeView(LayoutInflater.from(getContext())));
    }

    public void addItems(List<MDropItem> items) {
        mItems.addAll(items);
        for (MDropItem item : items) {
            addView(item.makeView(LayoutInflater.from(getContext())));
        }
    }

    public TYPE getType() {
        return mType;
    }

    public void setType(TYPE type) {
        mType = type;
        requestLayout();
    }

    public void setAlign(ALIGN align) {
        mAlign = align;
        requestLayout();
    }

    public ALIGN getAlign() {
        return mAlign;
    }

    public void setItemMargin(int margin) {
        mItemMargin = margin;
        requestLayout();
    }

    public boolean isVisible() {
        return isVisible;
    }

    public static class MDropItem {

        private int id;

        public MDropItem(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public View makeView(LayoutInflater inflater) {
            return null;
        }

        public View makeDescriptionView(LayoutInflater inflater) {
            return null;
        }

    }

    public static MDropDown show(Activity activity, View anchor, ArrayList<MDropItem> items) {
        final MDropDown mDropDown = new MDropDown(activity);
        mDropDown.setAnchorView(anchor);
        mDropDown.addItems(items);
        final View decorView = activity.getWindow().getDecorView();
        final PopupWindow popupWindow = new PopupWindow();
        popupWindow.setWidth(decorView.getWidth());
        popupWindow.setHeight(decorView.getHeight());
        popupWindow.setContentView(mDropDown);
        popupWindow.setFocusable(true);
        mDropDown.setPopupWindow(popupWindow);
        popupWindow.showAtLocation(anchor, Gravity.NO_GRAVITY, 0, 0);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mDropDown.toggle();
            }
        }, 150);
        return mDropDown;
    }

}
