package com.example.myapp;

import android.app.Activity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.*;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;

public class MyActivity extends Activity {

    private static final String TOP_LEFT = "top_left";
    private static final String TOP_RIGHT = "top_right";
    private static final String BOTTOM_LEFT = "bottom_left";
    private static final String BOTTOM_RIGHT = "bottom_right";

    private static final String TOGGLE_ALIGN = "toggle align";

    private static final String TOGGLE = "toggle";

    MDropDown mDropDown;
    MDropDown mDropDropDown;
    private Button mBtn;
    private ArrayList<MDropDown.MDropItem> mDropItems = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mBtn = (Button) findViewById(R.id.btn);
        mDropDown = (MDropDown) findViewById(R.id.drop);
        mDropDown.setAnchorView(findViewById(R.id.btn));

        mDropItems.add(new TestDropItem(1));
        mDropItems.add(new TestDropItem(2));
        mDropItems.add(new TestDropItem(3));
        mDropItems.add(new TestDropItem(4));
        mDropItems.add(new TestDropItem(5));
        mDropItems.add(new TestDropItem(6));

        mDropDown.addItems(mDropItems);

        findViewById(R.id.toggle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDropDown.toggle();
            }
        });

        findViewById(R.id.drop_down).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDropDropDown = MDropDown.show(MyActivity.this, mBtn, mDropItems);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(TOP_LEFT);
        menu.add(TOP_RIGHT);
        menu.add(BOTTOM_LEFT);
        menu.add(BOTTOM_RIGHT);
        menu.add(TOGGLE);
        menu.add(TOGGLE_ALIGN);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle().equals(TOGGLE_ALIGN)) {
            mDropDown.setAlign(mDropDown.getAlign() == MDropDown.ALIGN.CENTER ?
                    MDropDown.ALIGN.LEFT : mDropDown.getAlign() == MDropDown.ALIGN.LEFT ? MDropDown.ALIGN.RIGHT :
                    mDropDown.getAlign() == MDropDown.ALIGN.RIGHT ? MDropDown.ALIGN.TOP : mDropDown.getAlign() == MDropDown.ALIGN.TOP
                            ? MDropDown.ALIGN.BOTTOM : MDropDown.ALIGN.CENTER);
            Toast.makeText(this, "Align: " + mDropDown.getAlign(), Toast.LENGTH_SHORT).show();
            return true;
        }
        if (item.getTitle().equals(TOGGLE)) {
            mDropDown.setType(mDropDown.getType() == MDropDown.TYPE.HORIZONTAL ? MDropDown.TYPE.VERTICAL : MDropDown.TYPE.HORIZONTAL);
            return true;
        }

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, getResources().getDisplayMetrics());
        params.setMargins(margin, margin, margin, margin);
        if (item.getTitle().equals(TOP_LEFT)) {
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        } else if (item.getTitle().equals(TOP_RIGHT)) {
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        } else if (item.getTitle().equals(BOTTOM_LEFT)) {
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        } else if (item.getTitle().equals(BOTTOM_RIGHT)) {
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        }

        mBtn.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mDropDown.requestLayout();
                mBtn.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });

        mBtn.setLayoutParams(params);

        return true;
    }

    public static class TestDropItem extends MDropDown.MDropItem {

        public TestDropItem(int id) {
            super(id);
        }

        @Override
        public View makeView(LayoutInflater inflater) {
            final View v = inflater.inflate(R.layout.test_button, null);
            Button button = (Button) v.findViewById(R.id.test);
            button.setText(button.getText() + ": " + getId());
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(v.getContext(), "Clicked: " + getId(), Toast.LENGTH_SHORT).show();
                }
            });
            return v;
        }
    }

}
